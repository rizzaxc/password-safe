# Password safe

This is a Python script for securely storing password. Think Lastpass, but probably less sophisticated. Put a pair of username + password in and they're securely protected by public key cryptography. When the user needs to see the password, they're required to enter the passphrase used to generate the key pair.

## Getting started

### Prerequisites

The script requires Python 3 and [PyCA](https://pypi.org/project/pyca/) to work. Use `pip install pyca` to download the library.

You also need a self-signed X.509 certificate and its private key. You can do this to create one if you haven't:

> 1. Install openssl
> 2. `openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365`
> 3. Answer (or ignore) the questions about the content of the certificate.
> 4. Enter the passphrase for the private key. This will be needed when you access your passwords.
> 5. You have now created a 4096-bit RSA key pair, valid in 365 days.

If you're unfamiliar, further readings of the openssl library is encouraged. 

Note the default names for the certificate and the key in the script are *cert.pem* and *key.pem*.

### Using the tool

It has 3 options: *add*, *show*, *update*. Run this command to use:

`python3 safe.py -option $username`

There's also an *all* command to display every username it manages:

`python3 safe.py -all`

## Misc

### Author

- **Thach Kim Tran** - Sole author: this was originally a project for a university assignment. It's been modified to suit my personal liking.

### License

This project is licensed under the [MIT License](https://opensource.org/licenses/MIT).