# *** Written by Thach Tran ***
# **** Monash University ****

#The script is a command line tool for password management
#using public key cryptosystem

#!/usr/bin/env python3
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography import x509
from cryptography.hazmat.primitives.asymmetric import padding
import getpass
import argparse
import os
import os.path

#The path where the cert, key and passwords are stored
#Default to the code's location
directory = os.path.expanduser('./')

#X.509 certificate and its corresponding private key
pubk_name = 'cert.pem'
prvk_name = 'key.pem'

#Password extension
ext = '.pw'

#Read binary content from a file
def readfile_binary(file):
	with open(file,'rb') as f:
		content = f.read()
	return content

#Write binary content to a file
def writefile_binary(file,content):
	with open(file,'wb') as f:
		f.write(content)

	return True

#Return the public key located at cert.pem
def read_pubkey():
	file = directory+pubk_name
	pem_data = readfile_binary(file)

	cert = x509.load_pem_x509_certificate(pem_data, default_backend())
	public_key = cert.public_key()
	return (public_key)

#Return the private key located at private_key.pem
#User needs to enter passphrase
def read_prvkey():
	file = directory+prvk_name
	pem_data = readfile_binary(file)

	pass_phrase = getpass.getpass(prompt='Enter passphrase: ').encode('utf-8')

	prvkey = serialization.load_pem_private_key(
		pem_data,
		password = pass_phrase,
		backend = default_backend())

	return (prvkey)

#Encrypt and write the password to file
#Also used for updating
def do_add(pubkey,file,pass_to_store):
	encrypted_password = pubkey.encrypt(
		pass_to_store,
		padding.OAEP(
			mgf=padding.MGF1(algorithm=hashes.SHA1()),
			algorithm=hashes.SHA1(),
			label=None)
		)

	if (writefile_binary(file,encrypted_password)):
		print('Success!')

#Decrypt and show the password at file
def do_show(prvkey,file):
	encrypted_password = readfile_binary(file)

	password = prvkey.decrypt(
		encrypted_password,
		padding.OAEP(
			mgf=padding.MGF1(algorithm=hashes.SHA1()),
			algorithm=hashes.SHA1(),
			label=None)
		).decode('utf-8')
	print(password)


#Handle the logic for adding a password under a name
def add(file):
	pubkey = read_pubkey()
	password = getpass.getpass().encode('utf-8')
	do_add(pubkey,file,password)


#Handle the logic for showing a password under a name
def show(file):
	if (not os.path.isfile(directory+file)):
		print('No such user!')
		return
	try:
		prvkey = read_prvkey()
		do_show(prvkey,file)
	except ValueError:
		print('Incorrect passphrase!')

#Handle the logic for updating a password under a name
def update(file):
	if (not os.path.isfile(directory+file)):
		print('No such user!')
		return
	pubkey = read_pubkey()
	password = getpass.getpass().encode('utf-8')
	do_add(pubkey,file,password)

#Iterate through the folder, find every file with .pw and print the names
def show_all_names():
	for file in os.listdir(directory):
		if file.endswith('.pw'):
			print(file[:-3])

#Handle overall structure of the program
def main():
	parser = argparse.ArgumentParser(description='Password Manager')
	parser.add_argument('-add', help='Add a new username/password pair')
	parser.add_argument('-update', help='Update an existing username/password pair')
	parser.add_argument('-show', help='Show the password for this username')
	parser.add_argument('-all', help='Show all usernames', action='store_true')
	args = parser.parse_args()

	if (args.add and not args.show and not args.update and not args.all):
		add(args.add+ext)
	elif (args.show and not args.add and not args.update and not args.all):
		show(args.show+ext)
	elif (args.update and not args.add and not args.show and not args.all):
		update(args.update+ext)
	elif (args.all):
		show_all_names()
	else:
		print('Bad command!')



main()